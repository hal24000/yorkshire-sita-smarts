import urllib.parse

from pymongo import MongoClient

host = "10.0.1.4"
port = 27017

user_name = ""
pass_word = "" 

db_name = "cd_yorkshirewater"  # database name to authenticate

# if your password has '@' then you might need to escape hence we are using "urllib.parse.quote_plus()" 
client = MongoClient(f'mongodb://{user_name}:{urllib.parse.quote_plus(pass_word)}@{host}:{port}/{db_name}') 
db = client['cd_yorkshirewater']