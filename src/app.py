from datetime import datetime

import pandas as pd
import streamlit as st

from setup.layout import setup_page
from models.plot import (
    plot_bw_cycles,
    plot_diff_pres_prime,
    plot_rgf,
    plot_time_between_bw,
    plot_turbidity,
)

setup_page("Situational Awareness Further Investigation")


my_bar = st.progress(0)
st.header("Situational Awareness - RGF Backwash Preliminary Analysis")
st.sidebar.subheader("Select date")
with st.sidebar.form(key="my_form"):
    start_time = st.date_input(
        label="Start",
        value=datetime.strptime("2020/12/01", "%Y/%m/%d"),
        min_value=datetime.strptime("2020/11/27", "%Y/%m/%d"),
        max_value=datetime.strptime("2021/03/22", "%Y/%m/%d"),
    )
    end_time = st.date_input(
        label="End",
        value=datetime.strptime("2020/12/14", "%Y/%m/%d"),
        min_value=datetime.strptime("2020/11/27", "%Y/%m/%d"),
        max_value=datetime.strptime("2021/03/22", "%Y/%m/%d"),
    )
    submit_button = st.form_submit_button(label="Submit")

start_time = pd.to_datetime(start_time, format="%Y-%m-%d %H:%M:%S")
end_time = pd.to_datetime(end_time, format="%Y-%m-%d %H:%M:%S")

st.subheader("Site turbidity")
st.plotly_chart(plot_turbidity(start_time, end_time), use_containter_width=True)
my_bar.progress(20)

st.subheader("RGF 1")
st.plotly_chart(plot_rgf(start_time, end_time), use_containter_width=True)
my_bar.progress(40)

st.subheader("Backwash cycles")
st.plotly_chart(plot_bw_cycles(start_time, end_time), use_containter_width=True)
my_bar.progress(60)

st.subheader("Time between backwash (zoom excludes 2 severe outliers)")
st.plotly_chart(plot_time_between_bw(start_time, end_time), use_containter_width=True)
my_bar.progress(80)

st.subheader("Derivative of differential pressure")
st.plotly_chart(plot_diff_pres_prime(start_time, end_time), use_containter_width=True)
my_bar.progress(100)


with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: Info
    - Analysis of situational awareness data towards more advanced models.
    """
    )
