from datetime import datetime

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import streamlit as st
from plotly.subplots import make_subplots

from setup.database import db_con


db = db_con()


def plot_rgf(start_time, end_time):
    tag = "HUBYWTS1:RGF1_WASHING"
    query = {
        "tag_name": tag,
        "measurement_timestamp": {
            "$gte": start_time,
            "$lte": end_time,
        },
    }
    project = {"_id": 0, "measurement_timestamp": 1, "measurement_value": 1}
    df = (
        pd.DataFrame(db["alarm_values"].find(query, project))
        .set_index("measurement_timestamp")
        .sort_index()
    )
    all_df = df.copy()
    all_df = all_df.rename(columns={"measurement_value": "backwash"})
    #     rgf_values['bw_status'] = np.where(rgf_values['measurement_value']=="Inactive", 0, 1)
    #     df = rgf_values[["bw_status"]]

    tag_list = ["HUBYWTS1:RGF1_TB_SI", "HUBYWTS1:RGF1_DP_SI"]
    for tag in tag_list:
        query = {
            "tag_name": tag,
            "measurement_timestamp": {
                "$gte": start_time,
                "$lte": end_time,
            },
        }
        project = {"_id": 0, "measurement_timestamp": 1, "measurement_value": 1}
        df = (
            pd.DataFrame(db["sensor_values"].find(query, project))
            .set_index("measurement_timestamp")
            .sort_index()
        )
        all_df = pd.concat([all_df, df])
        all_df = all_df.rename(columns={"measurement_value": tag})

    #     df_feat['bw_status'] = df_feat['bw_status'].fillna(method="ffill").fillna(method="bfill")
    #     df_feat['measurement_value'] = df_feat['measurement_value'].interpolate()

    all_df["day"] = all_df.index.day
    all_df["hour"] = all_df.index.hour
    all_df["weekday"] = all_df.index.day_name()
    all_df["month"] = all_df.index.month

    #     df_feat['bw_status'] = df_feat['bw_status'].fillna(method="ffill").fillna(method="bfill")
    #     df_feat['measurement_value'] = df_feat['measurement_value'].interpolate()

    #     df_feat['turbidity'] = df_feat['turbidity'].interpolate()
    #     df_feat['turbidity'] = df_feat['turbidity'].fillna(method="bfill")

    #     df_feat['mask'] = (df_feat['bw_status'].shift () != df_feat['bw_status']) & (df_feat['bw_status']==1)
    #     df_feat['bw_on'] = np.where(df_feat['mask'], 1, 0)

    fig = make_subplots(
        rows=3,
        cols=1,
        vertical_spacing=0.03,
        shared_xaxes=True,
    )
    fig.add_trace(
        go.Scatter(x=all_df.index, y=all_df["backwash"], name="Backwash"), row=1, col=1
    )
    counter = 2
    for tag in tag_list:
        fig.add_trace(
            go.Scatter(x=all_df.index, y=all_df[tag], name=tag), row=counter, col=1
        )
        counter += 1

    fig.update_xaxes(
        range=[
            datetime.strftime(start_time, "%Y-%m-%d"),
            datetime.strftime(end_time, "%Y-%m-%d"),
        ]
    )
    fig["layout"].update(
        margin=dict(l=20, r=20, b=20, t=20),
        legend=dict(orientation="h", yanchor="bottom", y=-0.2, xanchor="center", x=0.5),
        width=1000,
        height=300,
    )
    return fig


def plot_time_between_bw(start_time, end_time):
    query = {}
    project = {"_id": 0}
    rain = (
        pd.DataFrame(db["topcliffe_rain"].find(query, project))
        .set_index("Datetime")
        .sort_index()
    )
    rain = rain[start_time:end_time]

    tag_list = []
    for i in range(1, 7):
        tag_list.append(f"HUBYWTS1:RGF{i}_WASHING")
    project = {"_id": 0}
    fig = make_subplots(
        rows=2,
        cols=1,
        vertical_spacing=0.03,
        shared_xaxes=True,
    )

    counter = 1
    for tag in tag_list:
        query = {
            "tag_name": tag,
            "measurement_timestamp": {
                "$gte": start_time,
                "$lte": end_time,
            },
        }
        df = (
            pd.DataFrame(db["alarm_values"].find(query, project))
            .set_index("measurement_timestamp")
            .sort_index()
        )
        df["bw_status"] = np.where(df["measurement_value"] == "Inactive", 0, 1)
        df["mask"] = (df["bw_status"].shift() != df["bw_status"]) & (
            df["bw_status"] == 1
        )
        df["bw_on"] = np.where(df["mask"], 1, 0)
        df = df[["bw_on"]]

        test = pd.DataFrame(df.loc[df["bw_on"] == 1].index.to_series().diff())
        test.columns = ["bw_gap"]
        test = test.dropna()
        test = test / pd.Timedelta(hours=1)

        fig.add_trace(
            go.Scatter(
                x=test.index, y=test["bw_gap"], name=f"RGF {counter}", mode="markers"
            ),
            row=1,
            col=1,
        )
        counter += 1
    fig.add_trace(
        go.Scatter(
            x=rain.index,
            y=rain["Precipitation"],
            name="Rain",
            mode="markers",
            marker=dict(color="skyblue", size=5),
        ),
        row=2,
        col=1,
    )
    fig["layout"].update(
        margin=dict(l=20, r=20, b=20, t=20),
        legend=dict(orientation="h", yanchor="bottom", y=-0.4, xanchor="center", x=0.5),
        width=1000,
        height=400,
    )
    fig.update_xaxes(range=[start_time, end_time])
    fig.update_yaxes(range=[-20, 100], row=1, col=1)

    return fig


def plot_bw_cycles(start_time, end_time):
    tag_list = []
    for i in range(1, 7):
        tag_list.append(f"HUBYWTS1:RGF{i}_WASHING")
    project = {"_id": 0}
    fig = make_subplots(
        rows=len(tag_list),
        cols=1,
        vertical_spacing=0.03,
        shared_xaxes=True,
    )
    counter = 1
    for tag in tag_list:
        query = {
            "tag_name": tag,
            "measurement_timestamp": {
                "$gte": start_time,
                "$lte": end_time,
            },
        }
        df = (
            pd.DataFrame(db["alarm_values"].find(query, project))
            .set_index("measurement_timestamp")
            .sort_index()
        )
        fig.add_trace(
            go.Scatter(x=df.index, y=df["measurement_value"], name=f"RGF {counter}"),
            row=counter,
            col=1,
        )
        counter += 1
    fig["layout"].update(
        legend=dict(orientation="h", yanchor="bottom", y=-0.1, xanchor="center", x=0.5),
        margin=dict(l=20, r=20, b=20, t=20),
        width=1000,
        height=100 * len(tag_list),
    )
    return fig


def plot_turbidity(start_time, end_time):
    query = {}
    project = {"_id": 0}
    rain = (
        pd.DataFrame(db["topcliffe_rain"].find(query, project))
        .set_index("Datetime")
        .sort_index()
    )
    rain = rain[start_time:end_time]

    tag_list = [
        "HUBYWTS1:RAW_WATER_TURBIDITY_SI",
        "HUBYWTS1:CLARIFIED_WATER_TURBIDITY_SI",
        "HUBYWTS1:RGF1_TB_SI",
        "HUBYWTS1:RGF_COMMON_TB_SI",
        "HUBYWTS1:GAC_DW_QIT890SCD",
        "HUBYWTS1:FINAL_WATER_TURBIDITY_SI",
    ]
    fig = make_subplots(
        rows=len(tag_list) + 2,
        cols=1,
        vertical_spacing=0.03,
        shared_xaxes=True,
    )
    fig.add_trace(
        go.Scatter(
            x=rain.index,
            y=rain["Precipitation"],
            name="Rain",
            mode="markers",
            marker=dict(color="skyblue", size=5),
        ),
        row=1,
        col=1,
    )

    project = {"_id": 0}
    query = {
        "tag_name": "HUBYWTS1:RGF1_WASHING",
        "measurement_timestamp": {
            "$gte": start_time,
            "$lte": end_time,
        },
    }
    rgf_values = (
        pd.DataFrame(db["alarm_values"].find(query, project))
        .set_index("measurement_timestamp")
        .sort_index()
    )
    fig.add_trace(
        go.Scatter(
            x=rgf_values.index,
            y=rgf_values["measurement_value"],
            name="HUBYWTS1:RGF1_WASHING",
        ),
        row=2,
        col=1,
    )

    counter = 3
    for tag in tag_list:
        query = {
            "tag_name": tag,
            "measurement_timestamp": {
                "$gte": start_time,
                "$lte": end_time,
            },
        }
        turbidity_df = (
            pd.DataFrame(db["sensor_values"].find(query, project))
            .set_index("measurement_timestamp")
            .sort_index()
        )
        fig.add_trace(
            go.Scatter(
                x=turbidity_df.index, y=turbidity_df["measurement_value"], name=tag
            ),
            row=counter,
            col=1,
        )
        counter += 1
    fig["layout"].update(
        legend=dict(
            orientation="h", yanchor="bottom", y=-0.15, xanchor="center", x=0.5
        ),
        margin=dict(l=20, r=20, b=20, t=20),
        width=1000,
        height=(len(tag_list) + 2) * 100,
    )
    fig.update_xaxes(
        range=[
            datetime.strftime(start_time, "%Y-%m-%d"),
            datetime.strftime(end_time, "%Y-%m-%d"),
        ]
    )
    return fig


def plot_diff_pres_prime(start_time, end_time):
    fig = make_subplots(
        rows=4,
        cols=1,
        vertical_spacing=0.03,
        shared_xaxes=True,
    )
    tag = "HUBYWTS1:RGF1_WASHING"
    query = {
        "tag_name": tag,
        "measurement_timestamp": {
            "$gte": start_time,
            "$lte": end_time,
        },
    }
    project = {"_id": 0}
    df = (
        pd.DataFrame(db["alarm_values"].find(query, project))
        .set_index("measurement_timestamp")
        .sort_index()
    )
    fig.add_trace(
        go.Scatter(x=df.index, y=df["measurement_value"], name=f"RGF 1"), row=1, col=1
    )

    sensor_list = ["HUBYWTS1:CLARIFIED_WATER_TURBIDITY_SI", "HUBYWTS1:RGF1_DP_SI"]

    project = {"_id": 0}
    counter = 2
    for tag in sensor_list:
        query = {
            "tag_name": tag,
            "measurement_timestamp": {
                "$gte": start_time,
                "$lte": end_time,
            },
        }
        project = {"_id": 0}
        df = (
            pd.DataFrame(db["sensor_values"].find(query, project))
            .set_index("measurement_timestamp")
            .sort_index()
        )
        fig.add_trace(
            go.Scatter(x=df.index, y=df["measurement_value"], name=tag),
            row=counter,
            col=1,
        )
        counter += 1

    df["value_prime"] = np.gradient(df["measurement_value"])
    fig.add_trace(
        go.Scatter(x=df.index, y=df["value_prime"], name="Differential pressure prime"),
        row=counter,
        col=1,
    )
    fig["layout"].update(
        legend=dict(orientation="h", yanchor="bottom", y=-0.2, xanchor="center", x=0.5),
        margin=dict(l=20, r=20, b=20, t=20),
        width=1000,
        height=400,
    )
    return fig


# def plot_backwash(start_time, end_time, text_feature):
#     query = {"site_name": "HUBY"}
#     project = {
#         "_id": 0,
#     }
#     site_config = pd.DataFrame(db["site_tag_config"].find(query, project))

#     rgf = site_config[
#         site_config["site_specific_schematic"] == "Rapid Gravity Filtration"
#     ].reset_index(drop=True)
#     rgf_text = rgf[
#         rgf["tag_description"].str.contains("1") & (rgf["measurement_unit"] == "Text")
#     ]["tag_name"][:-1].to_list()
#     rgf_no_text = rgf[
#         rgf["tag_description"].str.contains("1") & (rgf["measurement_unit"] != "Text")
#     ]["tag_name"][5:].to_list()

#     rgf_text_titles = rgf[
#         rgf["tag_description"].str.contains("1") & (rgf["measurement_unit"] == "Text")
#     ]["tag_short_description"][:-1].to_list()
#     rgf_no_text_titles = rgf[
#         rgf["tag_description"].str.contains("1") & (rgf["measurement_unit"] != "Text")
#     ]["tag_short_description"][5:].to_list()

#     query = {
#         "tag_name": "HUBYWTS1:RGF1_WASHING",
#         "measurement_timestamp": {
#             "$gte": start_time,
#             "$lte": end_time,
#         },
#     }
#     project = {
#         "_id": 0,
#     }
#     rgf_status = (
#         pd.DataFrame(db["alarm_values"].find(query, project))
#         .set_index("measurement_timestamp")
#         .sort_index()
#     )

#     if text_feature:
#         rgf_select = rgf_text
#         rgf_titles = rgf_text_titles
#         collection = "alarm_values"
#     else:
#         rgf_select = rgf_no_text
#         rgf_titles = rgf_no_text_titles
#         collection = "sensor_values"

#     subplot_titles = ["backwash_status"] + rgf_titles
#     fig = make_subplots(
#         rows=len(rgf_select) + 1,
#         cols=1,
#         subplot_titles=subplot_titles,
#         vertical_spacing=0.03,
#         shared_xaxes=True,
#     )
#     fig.add_trace(
#         go.Scatter(x=rgf_status.index, y=rgf_status["measurement_value"]), row=1, col=1
#     )
#     row_count = 2
#     for tag_name in rgf_select:
#         query = {
#             "tag_name": tag_name,
#             "measurement_timestamp": {
#                 "$gte": start_time,
#                 "$lte": end_time,
#             },
#         }
#         project = {
#             "_id": 0,
#         }
#         try:
#             tag_name_df = (
#                 pd.DataFrame(db[collection].find(query, project))
#                 .set_index("measurement_timestamp")
#                 .sort_index()
#             )
#             fig.add_trace(
#                 go.Scatter(x=tag_name_df.index, y=tag_name_df["measurement_value"]),
#                 row=row_count,
#                 col=1,
#             )
#         except:
#             pass
#         row_count += 1

#     fig["layout"].update(
#         margin=dict(l=20, r=20, b=0, t=20),
#         height=len(subplot_titles) * 100,
#         showlegend=False,
#     )
#     return fig
