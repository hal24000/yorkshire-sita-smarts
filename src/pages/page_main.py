import pandas as pd
import streamlit as st

from models.model import plot_cities
from setup.layout import setup_page


def run_page_main():
    st.header("Dimension StUI Template")
    with st.sidebar:
        button = st.button("Press here")
        checkbox = st.checkbox("Tick here")
    with st.sidebar.form(key="Form1"):
        radio = st.radio("Choose here", ["This", "That"])
        selectbox = st.selectbox("Select here", ["This", "That"])
        multiselect = st.multiselect("Select multi here", ["This", "That"])
        slider = st.slider("Slide here", 0, 100)
        selectslider = st.select_slider("Select slide here", ["This", "That"])
        text = st.text_input("Enter text here")
        number = st.number_input("Enter number here")
        textarea = st.text_area("Enter multi text here")
        date = st.date_input("Enter date here")
        time = st.time_input("Enter time here")
        file = st.file_uploader("Upload file here")
        color = st.color_picker("Pick color here")
        submitted1 = st.form_submit_button(label="Search 🔎")

    us_cities = pd.read_csv(
        "https://raw.githubusercontent.com/plotly/datasets/master/us-cities-top-1k.csv"
    )
    cities_fig = plot_cities(us_cities)

    st.subheader("US Cities")
    c1_1, c1_2 = st.columns(2)
    with c1_1:
        st.markdown("Column 1")
        st.dataframe(us_cities)
    with c1_2:
        st.markdown("Column 2")
        st.plotly_chart(cities_fig, use_containter_width=True)
