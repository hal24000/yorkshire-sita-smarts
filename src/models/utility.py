import pandas as pd

from conf.database import db


def find_dates():
    query = {
        "tag_name": "HUBYWTS1:RGF1_WASHING",
    }
    project = {
        "_id": 0,
    }
    min_date = pd.DataFrame(
        db.alarm_values.find(query, project).sort("measurement_value", -1).limit(1)
    )["measurement_timestamp"][0]
    max_date = pd.DataFrame(
        db.alarm_values.find(query, project).sort("measurement_value", 1).limit(1)
    )["measurement_timestamp"][0]
    return min_date, max_date
